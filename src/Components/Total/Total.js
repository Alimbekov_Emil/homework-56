import React from "react";
import "./Total.css";

const Total = (props) => {
  return <div className="Total">Total:{props.totalSpend()}</div>;
};

export default Total;
