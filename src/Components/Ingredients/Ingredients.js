import React from "react";
import "./Ingredients.css";
import DeleteButton from "../DeleteButton/DeleteButton";
import Counter from "../Counter/Counter";

const Ingredients = (props) => {
  return (
    <div className="Ingredients">
      <span onClick={props.counter} className="ProductName">
        {props.name} <b>+</b>
      </span>
      <span className="Price"> Цена: {props.price}</span>
      <Counter count={props.ing} name={props.name} />
      <DeleteButton onClick={props.clean} />
    </div>
  );
};

export default Ingredients;
