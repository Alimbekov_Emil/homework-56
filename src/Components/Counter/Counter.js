import React from "react";

const Counter = (props) => {
  const array = [];
  const ingName = props.count.filter((ing) => ing.name === props.name)[0];
  array.push(ingName.count);

  return <span>X{array}</span>;
};

export default Counter;
