import React from "react";
import "./DeleteButton.css";

const DeleteButton = (props) => {
  return (
    <button className="Btn-delete" onClick={props.onClick}>
      Delete
    </button>
  );
};

export default DeleteButton;
