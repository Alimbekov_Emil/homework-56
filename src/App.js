import React, { useState } from "react";
import "./App.css";
import Ingredients from "./Components/Ingredients/Ingredients";
import Order from "./Components/Order/Order";
import Total from "./Components/Total/Total";

const INGREDIENTS = [
  { name: "Meat", price: 50 },
  { name: "Cheese", price: 20 },
  { name: "Salad", price: 5 },
  { name: "Bacon", price: 30 },
];

const App = () => {
  const [ingredients, setIngredients] = useState([
    { name: "Meat", count: 0 },
    { name: "Cheese", count: 0 },
    { name: "Salad", count: 0 },
    { name: "Bacon", count: 0 },
    { name: "Burger", count: 1 },
  ]);

  const counter = (index) => {
    const ingredientsCopy = [...ingredients];
    ingredientsCopy[index].count++;
    setIngredients(ingredientsCopy);
  };

  const cleanOrder = (index) => {
    const ingredientsCopy = [...ingredients];
    if (ingredientsCopy[index].count !== 0) {
      ingredientsCopy[index].count--;
    }
    setIngredients(ingredientsCopy);
  };

  const menu = INGREDIENTS.map((ing, index) => {
    return (
      <Ingredients
        key={index}
        ing={ingredients}
        name={ing.name}
        price={ing.price}
        counter={() => counter(index)}
        clean={() => cleanOrder(index)}
      />
    );
  });

  const totalSpend = () => {
    return ingredients.reduce((acc, ing) => {
      if (ing.name.indexOf("Meat") !== -1) {
        return acc + ing.count * 50;
      } else if (ing.name.indexOf("Cheese") !== -1) {
        return acc + ing.count * 20;
      } else if (ing.name.indexOf("Bacon") !== -1) {
        return acc + ing.count * 30;
      } else if (ing.name.indexOf("Salad") !== -1) {
        return acc + ing.count * 5;
      } else if (ing.name.indexOf("Burger") !== -1) {
        return acc + ing.count * 20;
      }
      return acc;
    }, 0);
  };

  const getIngs = (ingName) => {
    const arr = [];
    const ing = ingredients.filter((ing) => ing.name === ingName)[0];
    const count = ing.count;
    for (let i = 0; i < count; i++) {
      arr.push(<Order key={ingName + arr.length} className={ingName} />);
    }
    return arr;
  };

  return (
    <div className="App">
      <div className="Order">{menu}</div>
      <div className="ingredients">
        <div className="BreadTop">
          <div className="Seeds1"></div>
          <div className="Seeds2"></div>
        </div>
        {getIngs("Salad")}
        {getIngs("Meat")}
        {getIngs("Cheese")}
        {getIngs("Bacon")}
        <div className="BreadBottom"></div>
      </div>
      <Total totalSpend={totalSpend} />
    </div>
  );
};

export default App;
